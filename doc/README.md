Julius Mazer
User: jsmazer
ID: 1408159
CMPE156-Lab1

Files:

client_tcp.c: This is the client source code that accepts two arguments as
its parameters, the IP address and the port number respectively. One unique
attribute about my client that it prints the line "Logging Off" after
the client types exit. 

server_tcp.c: This is server source code that accepts one argument the port
number. One unique attribute about my server is that once the client makes
a conneciton with the server it prints "Connecting to client on port xxxx
and IP xxx.xxx.xxx.xxx." A second unique attribute that my server implements is
after the client types exit the server prints "Cutting connection off with client."
A third unique attribute that the server implements is when the client types a bad
command the server prints "user_typed_command: command not found." Finally, the server
is kept open and the client is able to reconnect to it any time.

lab1script.py: This is a test script that uses pexpect to automatically test the following
commands: date, df -h, du, cat /etc/resolv.conf, echo Hello, and ls. Along with pexpect
the test script also uses the unittest module. I give the unittest module a verbosity of 2
so that the user is able to easily see which tests pass.
ex). test_ls (__main__.TestClient) ... ok
->To run the lab1.script all the user has to do is ./lab1script.py

Makefile: The makefile compiles client_tcp.c in the /src folder and server_tcp.c in the
/src folder when the user tpyes make. When the user types in make clean the binaries are
removed from the bin file. Finally, to run the lab1script.py the user can type make script.

