#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int client;
	char input[256];
	input[255] = '\0';
	char output[1024];
	output[1023] = '\0';

    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2]));
    
	if(inet_pton(AF_INET, argv[1] , (&serv_addr.sin_addr)) <= 0)
	{
		perror("IP Not Addressable Error:");
		exit(-1);
	}

    memset(serv_addr.sin_zero, '\0', sizeof(serv_addr.sin_zero));

	if( (client = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		perror("Socket Creation Error: ");
		exit(-1);
	}

	if(connect(client, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		perror("Connection Error: ");
		exit(-1);
	}

	for(;;)
	{
		printf("client $ ");
		fgets(input, 256, stdin);

		
		if(strncmp(input, "exit\n", 5) == 0)
		{
			printf("Logging off\n");
			send(client, input, 256, 0);
			close(client);
			exit(1);
		}

		send(client, input, 256, 0);
		
		while(1)
		{
			int datalen = recv(client, output, 1024, 0);
			if(datalen)
			{
				if(strcmp(output, "Finished") == 0)
				{
					break;
				}
				else if(strcmp(output, "Bad command") == 0)
				{
				    printf("Server could not process bad command, please try another\n");
				    break;
				}
				else
				{
					printf("%s", output);
				}
			}
			else
			{
				fprintf(stderr, "%s", "Part of message never received, please execute command again!\n");
				break;
			}
		}

		memset(input, 0, 256);
		memset(output, 0, 1024);
	}
}
