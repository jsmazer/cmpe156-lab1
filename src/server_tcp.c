#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#define max_connections 1

FILE *result;

int main(int argc, char **argv)
{

    /* Remember you have to enter the port number on ther server side */

    int server, client;
    char buffer[256];
	buffer[255] = '\0';
    char output[1024];
	output[1023] ='\0';

    struct sockaddr_in serv_addr, client_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[1]));
    serv_addr.sin_addr.s_addr = INADDR_ANY; //inet_addr("127.0.0.1")
    memset(serv_addr.sin_zero, '\0', sizeof(serv_addr.sin_zero));

    if ( (server = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        perror("Socket Creation Error: ");
        exit(-1);
    }

	int reuse = 1;
	if (setsockopt(server, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
		perror("setsockopt(SO_REUSEADDR) failed");

	if (setsockopt(server, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(reuse)) < 0) 
		perror("setsockopt(SO_REUSEPORT) failed");

    if(bind (server, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
    	perror("Bind Error: ");
    	exit(-1);
    }

    for(;;)
    {
        if(listen (server, max_connections) < 0)
        {
        	perror("Listen Error: ");
        	exit(-1);
        }

        socklen_t client_addr_len = sizeof(client_addr);
        if( (client = accept(server, (struct sockaddr *)&client_addr, &client_addr_len)) < 0)
        {
            perror("Accept Error: ");
            exit(-1);
        }

        printf("Connecting to client on port %d and IP %s\n",
            ntohs(client_addr.sin_port), inet_ntoa(client_addr.sin_addr));
        
        for(;;)
        {
            int datalen = recv(client, buffer, 256, 0);

            if(datalen)
            {
                if(strncmp(buffer, "exit\n", 5) == 0)
                {
                    printf("Cutting connection off with client\n");
                    close(client);
                    break;
                }
                else
                {
					//printf("Buffer: %s\n", buffer);
                    result = popen(buffer, "r");
					if(result != NULL)
                    {
                        while(fgets(output, 1024, result) != NULL)
                        {
                            send(client, output, 1024, 0);
                        }
                        strcpy(output, "Finished"); // Acknoledgement saying that all data has been sent
                        send(client, output, 1024, 0);
                    }

					pclose(result);
                    memset(buffer, 0, 256);
                    memset(output, 0, 1024);  
                }
            }
			else
				fprintf(stderr, "%s", "Message never received!\n");
        }

    }
}

