#!/usr/bin/env python3

import os
# library for controlling subprocesses
from pexpect import spawn, run
# library for unit testing
import unittest as ut
# library for temporary files/directories
import tempfile as tf

port = "1234"
ip = "127.0.0.1"
server_name = os.path.abspath(os.path.join(os.getcwd(), "../bin/server_tcp"))
client_name = os.path.abspath(os.path.join(os.getcwd(), "../bin/client_tcp"))

prompt = "client \$ "

class TestClient(ut.TestCase):
	
	# called before each test
	def setUp(self):
		self.wdir = tf.TemporaryDirectory()
		
		# creates a subprocess for the server
		# cwd changes the work directory for the process
		self.server = spawn(server_name + " " + port, cwd=self.wdir.name)
		
		# creates the client subprocess
		# echo true means that the client repeats the input in the output
		self.client = spawn(" ".join([client_name, ip, port]), echo=False)
		
		# populates the temporary directory
		run("touch file1 test2 extra Extra", cwd=self.wdir.name)
	
	# called after each test
	def tearDown(self):
		try:
			self.wdir.cleanup()
			self.server.terminate()
			self.client.terminate()
		except:
			self.assertFail()

	# all methods started with "test_" are tests and will be called in the main
	def test_date(self):
		self.client.expect(prompt)
		self.client.sendline("date")
		self.client.expect(prompt)
		result = self.client.before
		expected = run("date", cwd=self.wdir.name)
		self.assertEqual(result.split(), expected.split())

	def test_df_h(self):
		self.client.expect(prompt)
		self.client.sendline("df -h")
		self.client.expect(prompt)
		result = self.client.before
		expected = run("df -h", cwd=self.wdir.name)
		self.assertEqual(result.split(), expected.split())
	
	def test_du(self):
		self.client.expect(prompt)
		self.client.sendline("du")
		self.client.expect(prompt)
		result = self.client.before
		expected = run("du", cwd=self.wdir.name)
		self.assertEqual(result.split(), expected.split())

	def test_cat_conf(self):
		self.client.expect(prompt)
		self.client.sendline("cat /etc/resolv.conf")
		self.client.expect(prompt)
		result = self.client.before
		expected = run("cat /etc/resolv.conf", cwd=self.wdir.name)
		# print("Expected: {}".format(expected))
		self.assertEqual(result.split(), expected.split())

	def test_echo_hello(self):
		self.client.expect(prompt)
		self.client.sendline("echo HELLO")
		self.client.expect(prompt)
		result = self.client.before
		expected = run("echo HELLO", cwd=self.wdir.name)
		# print("Expected: {}".format(expected))
		self.assertEqual(result.split(), expected.split())

	def test_ls(self):
		self.client.expect(prompt)
		self.client.sendline("ls")
		self.client.expect(prompt)
		result = self.client.before
		expected = run("ls", cwd=self.wdir.name)
		self.assertEqual(result.split(), expected.split())

if __name__ == "__main__":
		# this runs the 'test_*' methods of the 'TestCase' classes
		ut.main(verbosity=2)

